## - battcn-starter-swagger ##

简化SpringBoot中繁琐的配置，自动装配，依赖即用

建议是用JDK1.8 SpringBoot1.5.4

	<dependency>
		<groupId>com.battcn</groupId>
		<artifactId>battcn-starter-swagger</artifactId>
		<version>1.0.1</version>
	</dependency>

<!-- config -->
	#以下就是需要写的配置，注意base-package就可以了	
	swagger:
	  enable: true	#是否开启Swagger
	  api-info:
	    description: battcn-plus	
	    license: battcn-plus
	    license-url: http://www.battcn.com
	    terms-of-service-url: http://www.battcn.com
	    title: 鏖战八方
	    version: 2.5.1
	    contact:
	      email: 1837307557@qq.com
	      name: Levin
	      url: http://www.battcn.com
	  docket:
	    base-package: com.battcn.platform.controller	#扫描路径，建议以Controller的父包为主
	    group-name: battcn-manage  


PS：比如A项目使用了 `battcn-starter-swagger` 那么只需要输入 http://${host}:${port}/swagger-ui.html 即可


博客：[http://blog.battcn.com/](http://blog.battcn.com/ "http://blog.battcn.com/")
